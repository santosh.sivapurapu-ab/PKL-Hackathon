from urllib.parse import urlencode
from urllib.request import Request, urlopen
import sys
import csv
import json

email = 'santosh.cbit2002@gmail.com'
json_file=open('season_7_match_data.json')
json_data=json.load(json_file)

sys.stdout.write('Status: 200 OK\n')
sys.stdout.write('Content-Type: text/csv; charset=utf-8\n')

url = 'https://json-csv.com/api/getcsv'
post_fields = {'email': email, 'json': json_data}

request = Request(url, urlencode(post_fields).encode())

csv_data = urlopen(request).read().decode()

csv_file = open('Season_7_Match_Detail.csv', 'w')
csv_file.write(csv_data)
csv_file.close()
